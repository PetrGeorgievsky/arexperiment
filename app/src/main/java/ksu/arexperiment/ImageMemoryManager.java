package ksu.arexperiment;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import java.util.ArrayList;

class ImageMemoryManager {

    private static ImageMemoryManager mInstance;
    private ArrayList<Mat> mTempBuffers = new ArrayList<>();
    private ArrayList<Mat> mTempGrayBuffers = new ArrayList<>();
    private Size mWorkingSize = new Size(512, 512);

    static ImageMemoryManager Instance() {
        if (mInstance == null)
            mInstance = new ImageMemoryManager();
        return mInstance;
    }

    Mat GetTempBuffer(int n) {
        if (mTempBuffers.size() <= n)
            mTempBuffers.add(new Mat(mWorkingSize, CvType.CV_32FC4));
        return mTempBuffers.get(n);
    }

    Mat GetGrayTempBuffer(int n) {
        if (mTempGrayBuffers.size() <= n)
            mTempGrayBuffers.add(new Mat(mWorkingSize, CvType.CV_32FC1));
        return mTempGrayBuffers.get(n);
    }
}
