package ksu.arexperiment;

import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

class Line {
    private double xDir, yDir;

    Line(Point a, Point b) {
        xDir = a.x - b.x;
        yDir = a.y - b.y;
        double norm = Math.sqrt(xDir * xDir + yDir * yDir);
        xDir /= norm;
        yDir /= norm;
    }

    boolean IsParallel(Line b) {
        double dp = Math.abs(b.xDir * xDir + b.yDir * yDir);
        return dp >= 0.99;
    }

    Point GetDir() {
        return new Point(xDir, yDir);
    }
}

// Класс для поиска якоря:
// На вход берет кадр с камеры, будет возвращать список найденых якорей,
// их вектора нормалей и приближенную дистанцию до камеры
// TODO: Добавить больше отладочного функционала, возможно стоит сделать часть конвеера отдельными объектами
class AnchorSearchPipeline {
    // Порог для фильтра кэнни
    private double mCannyThreshold = 10.0;
    // Изначальный размер изображения, используется для восстановления пропорций
    private Size mFrameBufferSize;
    // Ядро размытия для высокочастотных черт на изображении
    private Size mHighFreqBlurSize = new Size(3, 3);
    // Минимальная площадь контура якоря на изображении
    private int mMinContourArea = 500;

    void SetCannyTreshold(double value) {
        mCannyThreshold = value;
    }

    Mat Execute(Mat inputImage) {
        mFrameBufferSize = inputImage.size();

        // Уменьшаем изображение на котором будем искать контуры
        Mat downscaledImage = DownscaleImage(inputImage);
        // Фильтруем изображение от ненужных шумов
        Mat filteredImage = FilterImage(downscaledImage);

        // Находим контуры на отфильтрованном изображении
        List<MatOfPoint> contours = SearchContours(filteredImage);
        // TODO: Определять наиболее подходящие контуры
        // Рисуем контуры для отладки
        return DrawContours(inputImage, contours);
    }

    // Понижает размер изображения
    private Mat DownscaleImage(Mat inputImage) {
        // TODO: Понижать разрешение кадра в ближайшие степени 2ки, а не до 512*512
        Mat scaled_fb = ImageMemoryManager.Instance().GetTempBuffer(0);
        Imgproc.resize(inputImage, scaled_fb, scaled_fb.size());
        return scaled_fb;
    }

    private Mat FilterImage(Mat inputImage) {
        ImageMemoryManager memoryManager = ImageMemoryManager.Instance();

        Mat edges = memoryManager.GetTempBuffer(1);
        Mat grayscale = memoryManager.GetGrayTempBuffer(0);
        /*
        TODO: Подумать над более корректной фильтрацией изображения,
         чтобы добиться более качественного поиска в плохих условиях освещенности
        */
        Imgproc.cvtColor(inputImage, grayscale, Imgproc.COLOR_RGB2GRAY);
        Imgproc.blur(grayscale, grayscale, mHighFreqBlurSize);

        // FIXME: Вытащить обработку фильтром Кенни из данного метода?
        Imgproc.Canny(grayscale, edges, mCannyThreshold, mCannyThreshold * 3);

        return edges;
    }

    private List<MatOfPoint> SearchContours(Mat image) {
        List<MatOfPoint> contours = new ArrayList<>();
        List<MatOfPoint2f> res_contours = new ArrayList<>();

        // Ищем все контуры на изображении, пока что все без учета иерархии
        Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        MatOfInt hull = new MatOfInt();

        for ( MatOfPoint contour : contours ) {
            if (Imgproc.contourArea(contour) <= mMinContourArea)
                continue;

            MatOfPoint2f hull_contour = new MatOfPoint2f();

            Point[] contour_arr = contour.toArray();

            // Получаем выпуклый многоугольник на основе чистого контура.
            // ! Вот есть одна странность в АПИ ocv для жавы - многоугольник возвращается
            // ! как индексы в массив точек контура
            Imgproc.convexHull(contour, hull);

            int[] hull_arr = hull.toArray();

            Point[] convexContour = new Point[hull_arr.length];
            // Восстанавливаем контур при этом восстанавливая пропорции для дальнейшей отрисовки на экране
            for ( int i = 0; i < hull_arr.length; i++ ) {
                convexContour[i] = contour_arr[hull_arr[i]];
                convexContour[i].x *= mFrameBufferSize.width / 512;
                convexContour[i].y *= mFrameBufferSize.height / 512;
            }

            hull_contour.fromArray(convexContour);

            double length = Imgproc.arcLength(hull_contour, true);
            // Аппроксимируем многоугольник, чтобы точек стало еще меньше
            Imgproc.approxPolyDP(hull_contour, hull_contour, 0.1 * length, true);

            // Все получившиеся четырехугольники - нужные контуры
            if (hull_contour.rows() == 4)
                res_contours.add(hull_contour);

        }
        // Очередная придурь жавы
        contours = new ArrayList<>();
        for ( MatOfPoint2f conour : res_contours ) {
            contours.add(new MatOfPoint(conour.toArray()));
        }
        return contours;
    }

    // Рисует контуры поверх изображения, красным цветом
    private Mat DrawContours(Mat framebuffer, List<MatOfPoint> contours) {

        for ( int i = 0; i < contours.size(); i++ ) {

            Point[] contour = contours.get(i).toArray();
            Point midPoint = new Point((contour[0].x + contour[1].x + contour[2].x + contour[3].x) / 4, (contour[0].y + contour[1].y + contour[2].y + contour[3].y) / 4);
            // Ищем все почти-параллельные пары линий - они будут являться касательными к нашим прямоугольникам
            Line AB = new Line(contour[0], contour[1]);
            Line AC = new Line(contour[0], contour[2]);
            Line AD = new Line(contour[0], contour[3]);
            Line BD = new Line(contour[1], contour[3]);
            Line BC = new Line(contour[1], contour[2]);
            Line CD = new Line(contour[2], contour[3]);
            Point[] ss_tangents = new Point[2];
            int current_tangent = 0;

            if (AB.IsParallel(CD)) {
                ss_tangents[current_tangent] = AB.GetDir();
                current_tangent++;
            }
            if (AC.IsParallel(BD)) {
                ss_tangents[current_tangent] = AC.GetDir();
                current_tangent++;
            }
            // Вот тут может сломаться? вроде не должно
            if (AD.IsParallel(BC))
                ss_tangents[current_tangent] = AD.GetDir();

            // Рисуем касательные к прямоугольникам
            if (ss_tangents[0] != null)
                Imgproc.line(framebuffer, midPoint, new Point(midPoint.x + ss_tangents[0].x * 32, midPoint.y + ss_tangents[0].y * 32), new Scalar(0, 0, 255, 255), 8);
            if (ss_tangents[1] != null)
                Imgproc.line(framebuffer, midPoint, new Point(midPoint.x + ss_tangents[1].x * 32, midPoint.y + ss_tangents[1].y * 32), new Scalar(0, 255, 0, 255), 8);
            Imgproc.drawContours(framebuffer, contours, i, new Scalar(255, 0, 0, 255));

        }
        return framebuffer;
    }
}

