package ksu.arexperiment;

import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public class UdpRecieverThread implements Runnable {
    public SparseArray<MatOfPoint> markerCorners = new SparseArray<>();
    public ArrayList<Integer> lastFrameMarkers = new ArrayList<>();
    private int mPort;
    private byte[] buffer;
    private boolean running = true;

    public UdpRecieverThread(int port) {
        mPort = port;
        buffer = new byte[65507];
    }

    @Override
    public void run() {
        try (DatagramSocket socket = new DatagramSocket(mPort)) {
            socket.setBroadcast(true);
            DatagramPacket packet = new DatagramPacket(buffer, 65507);
            while (running) {
                socket.receive(packet);

                SaveResult(packet.getData(), packet.getLength());
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void SaveResult(byte[] buffer, int len) {
        lastFrameMarkers.clear();
        String s = new String(buffer, 0, len, StandardCharsets.UTF_8);
        try {
            JSONObject jsonObject = new JSONObject(s);
            int detectedMarkersCount = jsonObject.getInt("DetectedMarkersCount");
            JSONArray markers = jsonObject.getJSONArray("Corners");
            JSONArray ids = jsonObject.getJSONArray("Ids");
            Point[] pt_list = new Point[4];

            synchronized (lastFrameMarkers) {
                for (int i = 0; i < detectedMarkersCount; i++) {
                    JSONArray corners = markers.getJSONArray(i);
                    int id = ids.getInt(i);
                    for (int k = 0; k < 4; k++) {
                        JSONObject pt = corners.getJSONObject(k);
                        pt_list[k] = new Point(pt.getDouble("X"), pt.getDouble("Y"));
                    }
                    MatOfPoint cornersMat = new MatOfPoint();
                    cornersMat.fromArray(pt_list);
                    markerCorners.put(id, cornersMat);

                    lastFrameMarkers.add(id);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
