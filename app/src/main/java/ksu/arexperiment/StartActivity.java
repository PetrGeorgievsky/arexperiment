package ksu.arexperiment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class StartActivity extends Activity implements View.OnClickListener {
    private EditText mServerAddress;
    private Button mStartButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.start_activity);
        mServerAddress = findViewById(R.id.set_ip);
        mStartButton = findViewById(R.id.start_button);
        mStartButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.start_button) {
            ServerProperties.serverAddress = mServerAddress.getText().toString();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
