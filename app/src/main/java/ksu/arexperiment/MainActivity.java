package ksu.arexperiment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements CvCameraViewListener2 {
    private static final String TAG = "OCVSample::Activity";

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private CameraBridgeViewBase mOpenCvCameraView;
    private Mat mFrameBuffer, mTransposedFB;
    private AnchorSearchPipeline mAnchorSearchPipe = new AnchorSearchPipeline();
    private double avg_time = 0;
    private long tick = 0;
    ServerConnection serverConnection;
    UdpSender udpSender;
    UdpRecieverThread udpRecieverThread;
    Thread recieverThread;
    Drawer mDrawer;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }

        udpRecieverThread = new UdpRecieverThread(45869);
        recieverThread = new Thread(udpRecieverThread);
        recieverThread.start();

        mOpenCvCameraView = findViewById(R.id.view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setMaxFrameSize(128, 128);
        mOpenCvCameraView.enableFpsMeter();
        mOpenCvCameraView.setCvCameraViewListener(this);
        mDrawer = new Drawer(getResources());
        mOpenCvCameraView.mDrawer = mDrawer;

        mTransposedFB = new Mat(128, 128, CvType.CV_8UC3);

        serverConnection = new ServerConnection(ServerProperties.serverAddress, 49158);
        serverConnection.InitAuthToken("test");
        WifiManager wifiManager = (WifiManager) getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        String client_address = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        InetSocketAddress reciverAddress = serverConnection
                .InitVideoStreamAsync("testScene", client_address, 45869);
        udpSender = new UdpSender();
        UdpSender.mRecieverAddress = reciverAddress;
        UdpSender.mData = new byte[(int) (mTransposedFB.total() *
                mTransposedFB.channels())];
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        //mTransposedFB = new Mat(height, height, CvType.CV_32FC4);
    }

    @Override
    public void onCameraViewStopped() {
    }

    @Override
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mFrameBuffer = inputFrame.rgba();

        Imgproc.resize(mFrameBuffer, mTransposedFB, mTransposedFB.size());
        Core.rotate(mTransposedFB, mTransposedFB, Core.ROTATE_90_CLOCKWISE);
        Imgproc.cvtColor(mTransposedFB, mTransposedFB, Imgproc.COLOR_BGRA2RGB);
        MatOfByte mb = new MatOfByte();
        Imgcodecs.imencode(".jpg", mTransposedFB, mb);
        UdpSender.mData = mb.toArray();
        //mTransposedFB.get(0, 0, UdpSender.mData);
        udpSender = new UdpSender();
        udpSender.Send();


        //if (mTransposedFB != null)
        //    processFrame(mTransposedFB.nativeObj);
        synchronized (udpRecieverThread.lastFrameMarkers) {
            mDrawer.setAttrs(new ArrayList<>(udpRecieverThread.lastFrameMarkers),
                    udpRecieverThread.markerCorners.clone());

            for (int i = 0; i < udpRecieverThread.lastFrameMarkers.size(); i++) {
                int markerId = udpRecieverThread.lastFrameMarkers.get(i);
                List<MatOfPoint> ptList = new ArrayList<>();
                ptList.add(udpRecieverThread.markerCorners.get(markerId));
                Imgproc.drawContours(mTransposedFB, ptList, 0, new Scalar(0, 255, 0, 128));
            }
        }
        Imgproc.resize(mTransposedFB, mFrameBuffer, mFrameBuffer.size());
        return mFrameBuffer;
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native void processFrame(long inputFrame);

    public native void setCannyThreshold(double value);

    public native void setThreshold(int value);

    public native void setFilterStage(int value);

    @TargetApi(Build.VERSION_CODES.M)
    private void requestCameraPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            new ConfirmationDialog().show(getFragmentManager(), "dialog");
        } else {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
        }
    }

    public static class ConfirmationDialog extends DialogFragment {
        private static Fragment parent;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            parent = getParentFragment();

            return new AlertDialog.Builder(getActivity())
                    .setMessage("This app needs camera permission")
                    .setPositiveButton("OK", new PositiveListener())
                    .setNegativeButton("Cancel", new NegativeListener())
                    .create();
        }

        private class PositiveListener implements DialogInterface.OnClickListener {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                parent.requestPermissions(new String[]{Manifest.permission.CAMERA},
                        1);
            }
        }

        private class NegativeListener implements DialogInterface.OnClickListener {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Activity activity = parent.getActivity();

                if (activity != null) {
                    activity.finish();
                }
            }
        }
    }
}
