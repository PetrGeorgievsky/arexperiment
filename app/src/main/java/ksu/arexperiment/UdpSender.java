package ksu.arexperiment;

import android.os.AsyncTask;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Random;

public class UdpSender extends AsyncTask<Void, Void, Void> {
    static byte[] mData;
    static byte[] mRTPPacket = new byte[8];
    static byte[] mUDPPacket = new byte[65507];
    static InetSocketAddress mRecieverAddress;
    static int TimeStamp;
    static short SequenceNumber;
    static int UDP_PACKET_S = 65507;
    static int RTP_HEADER_S = 8;
    static int RTP_PL_S = UDP_PACKET_S - RTP_HEADER_S;

    static {
        Random rng = new Random();
        TimeStamp = rng.nextInt();
        SequenceNumber = (short) rng.nextInt(Short.MAX_VALUE + 1);
    }

    @Override
    protected Void doInBackground(Void... params) {
        DatagramSocket ds = null;

        try {
            ds = new DatagramSocket();
            DatagramPacket dp;
            dp = new DatagramPacket(mUDPPacket, mUDPPacket.length, mRecieverAddress);
            ds.setBroadcast(true);
            TimeStamp++;
            byte Version = 2;
            boolean HasPadding;
            boolean HasExtensions;
            boolean Marker = false;
            byte PayloadType = 112;
            mRTPPacket[0] = Version;

            // в первом байте пишем только версию, остальное возможно добавим позже
            //if (header.HasPadding)
            //    datagramm[0] |= 0b0000_0100;
            //
            mRTPPacket[1] = PayloadType;

            mRTPPacket[4] = (byte) (TimeStamp >> 24);
            mRTPPacket[5] = (byte) ((TimeStamp >> 16) & 0xFF);
            mRTPPacket[6] = (byte) ((TimeStamp >> 8) & 0xFF);
            mRTPPacket[7] = (byte) (TimeStamp & 0xFF);
            int packet_count = (mData.length + RTP_PL_S - 1) / RTP_PL_S;
            int last_packet_s = mData.length % RTP_PL_S;
            for (int i = 0; i < packet_count; i++) {
                // header
                mRTPPacket[2] = (byte) (SequenceNumber >> 8);
                mRTPPacket[3] = (byte) (SequenceNumber & 0xFF);
                SequenceNumber++;
                System.arraycopy(mRTPPacket, 0, mUDPPacket, 0, RTP_HEADER_S);


                int curr_packet_off = i * UDP_PACKET_S;
                int curr_packet_s = RTP_PL_S;
                if (i == packet_count - 1) {
                    curr_packet_s = last_packet_s;
                    mRTPPacket[1] |= 0b1000_0000;
                }
                System.arraycopy(mData, curr_packet_off, mUDPPacket, RTP_HEADER_S, curr_packet_s);

                dp.setData(mUDPPacket, 0, Math.min(UDP_PACKET_S, curr_packet_s + RTP_HEADER_S));
                ds.send(dp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ds != null) {
                ds.close();
            }
        }
        return null;
    }

    public void Send() {
        executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }
}
