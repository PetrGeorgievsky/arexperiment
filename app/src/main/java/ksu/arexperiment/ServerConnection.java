package ksu.arexperiment;

import android.os.AsyncTask;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;

import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

class JsonRPCTask extends AsyncTask<Pair<ServerConnection, JSONRPC2Request>, Void, JSONRPC2Response> {
    @Override
    protected JSONRPC2Response doInBackground(Pair<ServerConnection, JSONRPC2Request>... pairs) {
        if (android.os.Debug.isDebuggerConnected())
            android.os.Debug.waitForDebugger();
        for (Pair<ServerConnection, JSONRPC2Request> pair : pairs) {
            PrintWriter writer = null;
            BufferedReader reader = null;
            Socket socket = pair.first.GetTCPSocket();
            try {
                writer = new PrintWriter(socket.getOutputStream(), true);
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String content = pair.second.toJSONString();
            Log.d("TEST", content);

            //char[] result = new char[byteBuffer.remaining()];
            //byteBuffer.get(result);
            assert writer != null;
            writer.write(String.format("Content-Length: %d\r\n\r\n", content.length()));
            writer.write(content);
            writer.flush();
            //writer.print(  );

            try {
                String response_len = reader.readLine();
                reader.readLine();
                int length = Integer.parseInt(response_len.substring(response_len.lastIndexOf(':') + 2));
                char[] res = new char[length];
                reader.read(res, 0, length);
                writer.close();
                reader.close();
                socket.close();
                String json = new String(res);
                return JSONRPC2Response.parse(json);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONRPC2ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

class ServerConnection {
    private String mAddress;
    private int mPort;
    private String mAuthToken;

    public ServerConnection(String address, int port) {
        mAddress = address;
        mPort = port;
    }

    public Socket GetTCPSocket() {
        try {
            return new Socket(mAddress, mPort);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    InetSocketAddress InitVideoStreamAsync(String sceneId, String dataRecieveIP, int dataRecievePort) {
        JsonRPCTask task = new JsonRPCTask();

        Map<String, Object> params = new ArrayMap<>();
        params.put("authToken", mAuthToken);
        params.put("sceneId", sceneId);
        params.put("dataRecieveIP", dataRecieveIP);
        params.put("dataRecievePort", dataRecievePort);

        JSONRPC2Request request = new JSONRPC2Request("InitVideoStreamAsync", params, 1);
        task.execute(new Pair<ServerConnection, JSONRPC2Request>(this, request));
        try {
            JSONRPC2Response jsonrpc2Response = task.get();
            HashMap<String, Object> json = (HashMap<String, Object>) jsonrpc2Response.getResult();
            return new InetSocketAddress((String) json.get("Item1"), ((Long) json.get("Item2")).intValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    void InitAuthToken(String machineId) {
        JsonRPCTask task = new JsonRPCTask();

        Map<String, Object> params = new ArrayMap<>();
        params.put("machineId", machineId);

        JSONRPC2Request request = new JSONRPC2Request("GetAuthTokenAsync", params, 1);
        task.execute(new Pair<ServerConnection, JSONRPC2Request>(this, request));
        try {
            JSONRPC2Response jsonrpc2Response = task.get();
            mAuthToken = jsonrpc2Response.getResult().toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
