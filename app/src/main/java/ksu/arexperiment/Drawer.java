package ksu.arexperiment;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.SparseArray;

import org.opencv.android.CustomDrawer;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.List;

public class Drawer implements CustomDrawer {
    private List<Integer> ids;
    private SparseArray<MatOfPoint> corners;
    private SparseArray<Bitmap> bitmaps;
    private Paint paint = new Paint();
    private Resources resources;

    public Drawer(Resources resources) {
        this.resources = resources;
        this.bitmaps = new SparseArray<>();
        Bitmap bitmap = BitmapFactory.decodeResource(resources, R.drawable.marker0);
        bitmaps.append(0, Bitmap.createScaledBitmap(bitmap, 256, 256, false));
        bitmap = BitmapFactory.decodeResource(resources, R.drawable.marker1);
        bitmaps.append(1, Bitmap.createScaledBitmap(bitmap, 256, 256, false));
    }

    @Override
    public void onDraw(Canvas canvas, android.graphics.Point offset, android.graphics.Point size) {
        if (ids == null || corners == null) {
            return;
        }

        for (int i = 0; i < ids.size(); i++) {
            int markerId = ids.get(i);
            draw(canvas, markerId, corners.get(markerId), offset, size);
        }

        ids = null;
        corners = null;
    }

    public void setAttrs(List<Integer> ids, SparseArray<MatOfPoint> corners) {
        this.ids = ids;
        this.corners = corners;
    }

    private void draw(Canvas canvas, int id, MatOfPoint corners, android.graphics.Point offset, android.graphics.Point size) {
        Point[] points = corners.toArray();
        Point center = findCenter(points);
        Point center_s = new Point((center.x / 128.0f) * size.x + offset.x,
                (center.y / 128.0f) * size.y + offset.y);
        float scale = findRad(points, center) / 16.0f;
        Bitmap bitmap = bitmaps.get(id);

        if (bitmap == null) {
            return;
        }

        int imgw = (int) (scale * bitmap.getWidth());
        int imgh = (int) (scale * bitmap.getWidth());

        canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new Rect((int) center_s.x - imgw / 2,
                        (int) center_s.y - imgh / 2,
                        (int) center_s.x + imgw / 2,
                        (int) center_s.y + imgh / 2),
                paint);
    }

    private void clear(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }

    private Point findCenter(Point[] points) {
        double minX = points[0].x;
        double maxX = points[2].x;
        double minY = points[0].y;
        double maxY = points[2].y;

        for (Point point : points) {
            if (point.x < minX) {
                minX = point.x;
            } else if (point.x > maxX) {
                maxX = point.x;
            }

            if (point.y < minY) {
                minY = point.y;
            } else if (point.y > maxY) {
                maxY = point.y;
            }
        }

        return new Point((maxX + minX) / 2, (maxY + minY) / 2);
    }

    private float findRad(Point[] points, Point c) {
        float avg_dist = 0;
        for (Point point : points) {
            Point difference = new Point(c.x - point.x, c.y - point.y);
            avg_dist += Math.sqrt(difference.dot(difference));
        }

        return avg_dist / 4;
    }
}
