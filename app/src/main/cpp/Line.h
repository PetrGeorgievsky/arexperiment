//
// Created by peter on 14.11.2018.
//

#ifndef AREXPERIMENT_LINE_H
#define AREXPERIMENT_LINE_H

#include <opencv2/core.hpp>

class Line {

public:
    Line(cv::Point a, cv::Point b) {
        mDir.x = a.x - b.x;
        mDir.y = a.y - b.y;
        double norm = sqrt(mDir.x * mDir.x + mDir.y * mDir.y);
        mDir.x /= norm;
        mDir.y /= norm;
    }

    bool IsParallel(Line b) {
        double dp = fabs(b.mDir.x * mDir.x + b.mDir.y * mDir.y);
        return dp >= 0.99;
    }

    cv::Point2d GetDir() {
        return mDir;
    }

private:
    cv::Point2d mDir;
};

#endif //AREXPERIMENT_LINE_H
