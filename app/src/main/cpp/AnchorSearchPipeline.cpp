//
// Created by peter on 12.11.2018.
//
#include <opencv2/imgproc.hpp>
#include "AnchorSearchPipeline.h"
#include "Line.h"
#include "ARCodeCandidate.h"

bool AnchorSearchPipeline::mScratchSpaceInitialized = false;

cv::Mat AnchorSearchPipeline::mOriginalImg;
cv::Mat AnchorSearchPipeline::mScratchSpace;
cv::Mat AnchorSearchPipeline::mGrayScratchSpace;
double AnchorSearchPipeline::mCannyThreshold = 30.0;
int AnchorSearchPipeline::mThreshold = 50;

int ClosestPowerOf2(int val) {
    // Побитовые трюки, зато быстро)
    val--;
    val |= val >> 1;
    val |= val >> 2;
    val |= val >> 4;
    val |= val >> 8;
    val |= val >> 16;
    val++;
    return val >> 2;
}


cv::Mat *AnchorSearchPipeline::Downscale(const cv::Mat &inputFrame) {

    cv::resize(inputFrame, mScratchSpace, mScratchSpace.size());
    //cv::pyrDown( inputFrame, mScratchSpace, mScratchSpace.size() );
    cv::medianBlur(mScratchSpace, mScratchSpace, 3);

    return &mScratchSpace;
}

AnchorSearchPipeline &AnchorSearchPipeline::Instance() {
    static AnchorSearchPipeline instance;
    return instance;
}

cv::Mat &AnchorSearchPipeline::Execute(const cv::Mat &inputFrame) {
    Init(inputFrame.size());
    cv::resize(inputFrame, mOriginalImg, mOriginalImg.size());
    cv::Mat *filters[4];
    filters[0] = Downscale(inputFrame);
    filters[1] = FilterImage(*filters[0]);
    filters[2] = DetectEdges(*filters[1]);
    filters[3] = &mOriginalImg;
    auto contours = FindContours(*filters[2]);
    DrawContours(mOriginalImg, contours);
    return *filters[mCurrentFilterStage];
}

void AnchorSearchPipeline::Init(const cv::Size &scratchSpaceSize) {
    if (!mScratchSpaceInitialized) {
        int min_size_x = ClosestPowerOf2(scratchSpaceSize.width);
        int min_size_y = ClosestPowerOf2(scratchSpaceSize.height);
        mRescaleCoeffs = cv::Point2d(scratchSpaceSize.width * 1.0 / min_size_x,
                                     scratchSpaceSize.height * 1.0 / min_size_y);
        //if( min_size_y < min_size_x )
        //    min_size_x = min_size_y;
        cv::Size downscaled_size(min_size_x, min_size_y);
        mOriginalImg = cv::Mat(downscaled_size, CV_32FC4);
        mScratchSpace = cv::Mat(downscaled_size, CV_32FC4);
        mGrayScratchSpace = cv::Mat(downscaled_size, CV_32FC1);
        mScratchSpaceInitialized = true;
    }
}

cv::Mat *AnchorSearchPipeline::FilterImage(const cv::Mat &inputFrame) {
    mGrayScratchSpace = inputFrame.clone();
    cv::cvtColor(inputFrame, mGrayScratchSpace, CV_RGB2GRAY);
    cv::equalizeHist(mGrayScratchSpace, mGrayScratchSpace);
    cv::threshold(mGrayScratchSpace, mGrayScratchSpace, mThreshold, 255, CV_THRESH_TRUNC);

    return &mGrayScratchSpace;
}

cv::Mat *AnchorSearchPipeline::DetectEdges(const cv::Mat &inputFrame) {
    cv::Canny(inputFrame, mGrayScratchSpace, mCannyThreshold, mCannyThreshold * 3);
    return &mGrayScratchSpace;
}

std::vector<ARCodeCandidateIntrinsics>
AnchorSearchPipeline::FindContours(const cv::Mat &inputFrame) {
    std::vector<std::vector<cv::Point>> contours;
    std::vector<ARCodeCandidateIntrinsics> res_contours;
    cv::findContours(inputFrame, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
    for (int i = 0; i < contours.size(); ++i) {
        if (cv::contourArea(contours[i]) <= mMinContourArea)
            continue;
        /*if( !cv::isContourConvex(contours[i]) )
            continue;*/

        std::vector<cv::Point> hull;
        cv::convexHull(contours[i], hull);

        double length = cv::arcLength(hull, true);
        // Аппроксимируем многоугольник, чтобы точек стало еще меньше
        cv::approxPolyDP(hull, hull, 0.1 * length, true);

        // Все получившиеся четырехугольники - нужные контуры
        if (hull.size() == 4) {
            /*for (int j = 0; j < hull.size(); ++j) {
                hull[j].x = static_cast<int>(hull[j].x * mRescaleCoeffs.x);
                hull[j].y = static_cast<int>(hull[j].y * mRescaleCoeffs.y);
            }*/
            Line AB(hull[0], hull[1]);
            Line AC(hull[0], hull[2]);
            Line AD(hull[0], hull[3]);
            Line BD(hull[1], hull[3]);
            Line BC(hull[1], hull[2]);
            Line CD(hull[2], hull[3]);
            cv::Point2d ss_tangents[2];
            int current_tangent = 0;

            if (AB.IsParallel(CD)) {
                ss_tangents[current_tangent] = AB.GetDir();
                current_tangent++;
            }
            if (AC.IsParallel(BD)) {
                ss_tangents[current_tangent] = AC.GetDir();
                current_tangent++;
            }
            // Вот тут может сломаться? вроде не должно
            if (AD.IsParallel(BC)) {
                ss_tangents[current_tangent] = AD.GetDir();
                current_tangent++;
            }
            if (current_tangent < 2)
                continue;
            ARCodeCandidateIntrinsics candidateIntrinsics;
            candidateIntrinsics.mSSPoints = hull;
            candidateIntrinsics.mSSTangent = ss_tangents[0];
            candidateIntrinsics.mSSBiTangent = ss_tangents[1];
            res_contours.push_back(candidateIntrinsics);
        }
    }
    return res_contours;
}

void AnchorSearchPipeline::DrawContours(const cv::Mat &inputFrame,
                                        const std::vector<ARCodeCandidateIntrinsics> &contours) {
    for (int i = 0; i < contours.size(); ++i) {
        std::vector<std::vector<cv::Point>> contours_;
        contours_.push_back(contours[i].mSSPoints);
        cv::Moments contour_moments = cv::moments(contours[i].mSSPoints);
        cv::Point center_ = cv::Point((int) (contour_moments.m10 / contour_moments.m00),
                                      (int) (contour_moments.m01 / contour_moments.m00));
        int tangent_size = 32;
        cv::Point tn_pt = cv::Point(center_.x + (int) (contours[i].mSSTangent.x * tangent_size),
                                    center_.y + (int) (contours[i].mSSTangent.y * tangent_size));
        cv::Point btn_pt = cv::Point(center_.x + (int) (contours[i].mSSBiTangent.x * tangent_size),
                                     center_.y + (int) (contours[i].mSSBiTangent.y * tangent_size));

        cv::line(inputFrame, center_, tn_pt, cv::Scalar(0, 255, 0, 1), 2);
        cv::line(inputFrame, center_, btn_pt, cv::Scalar(0, 0, 255, 1), 2);
        cv::drawContours(inputFrame, contours_, 0, cv::Scalar(255, 0, 0, 1), 2);
    }
}

void AnchorSearchPipeline::SetCannyThreshold(double value) {
    mCannyThreshold = value;
}

void AnchorSearchPipeline::SetThreshold(int value) {
    mThreshold = value;
}

void AnchorSearchPipeline::SetFilterStage(int value) {
    mCurrentFilterStage = value;
}
