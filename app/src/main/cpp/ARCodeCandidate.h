//
// Created by peter on 14.11.2018.
//

#ifndef AREXPERIMENT_ARCODECANDIDATE_H
#define AREXPERIMENT_ARCODECANDIDATE_H

#include <opencv2/core.hpp>

/// Внутренние параметры искомого объекта( параллелограма )
struct ARCodeCandidateIntrinsics {
    std::vector<cv::Point> mSSPoints;
    // Касательные к плоскости в пространстве экрана
    cv::Point2d mSSTangent;
    cv::Point2d mSSBiTangent;
};


#endif //AREXPERIMENT_ARCODECANDIDATE_H
