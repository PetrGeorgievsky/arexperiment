#include <jni.h>
#include <string>
#include <opencv2/opencv.hpp>
#include "AnchorSearchPipeline.h"

extern "C"
JNIEXPORT void JNICALL
Java_ksu_arexperiment_MainActivity_setCannyThreshold(JNIEnv *env, jobject instance, jdouble value) {
    AnchorSearchPipeline::Instance().SetCannyThreshold(value);
}

extern "C"
JNIEXPORT void JNICALL
Java_ksu_arexperiment_MainActivity_setThreshold(JNIEnv *env, jobject instance, jint value) {
    AnchorSearchPipeline::Instance().SetThreshold(value);
}

extern "C"
JNIEXPORT void JNICALL
Java_ksu_arexperiment_MainActivity_setFilterStage(JNIEnv *env, jobject instance, jint value) {

    AnchorSearchPipeline::Instance().SetFilterStage(value);

}

extern "C"
{

jstring JNICALL
Java_ksu_arexperiment_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";

    return env->NewStringUTF(hello.c_str());
}

void JNICALL
Java_ksu_arexperiment_MainActivity_processFrame(
        JNIEnv *env,
        jobject /* this */,
        jlong inputFrameAddr) {
    cv::Mat &inputFrame = *(cv::Mat *) inputFrameAddr;
    AnchorSearchPipeline pipeline = AnchorSearchPipeline::Instance();

    cv::Mat &filtered = pipeline.Execute(inputFrame);

    cv::resize(filtered, inputFrame, inputFrame.size());
}

}