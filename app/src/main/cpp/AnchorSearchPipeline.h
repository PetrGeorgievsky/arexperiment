//
// Created by peter on 12.11.2018.
//

#ifndef AREXPERIMENT_ANCHORSEARCHPIPELINE_H
#define AREXPERIMENT_ANCHORSEARCHPIPELINE_H

#include <opencv2/core.hpp>
#include "ARCodeCandidate.h"

class AnchorSearchPipeline {
public:
    static AnchorSearchPipeline &Instance();

    cv::Mat &Execute(const cv::Mat &inputFrame);

    void SetCannyThreshold(double value);

    void SetThreshold(int value);

    void SetFilterStage(int value);

private:
    void Init(const cv::Size &scratchSpaceSize);

    cv::Mat *Downscale(const cv::Mat &inputFrame);

    cv::Mat *FilterImage(const cv::Mat &inputFrame);

    cv::Mat *DetectEdges(const cv::Mat &inputFrame);

    std::vector<ARCodeCandidateIntrinsics> FindContours(const cv::Mat &inputFrame);

    void
    DrawContours(const cv::Mat &inputFrame, const std::vector<ARCodeCandidateIntrinsics> &contours);

private:
    static bool mScratchSpaceInitialized;
    static double mCannyThreshold;
    static int mThreshold;
    int mMinContourArea = 500;
    int mCurrentFilterStage = 3;
    cv::Point2d mRescaleCoeffs;
    static cv::Mat mOriginalImg;
    static cv::Mat mScratchSpace;
    static cv::Mat mGrayScratchSpace;
};


#endif //AREXPERIMENT_ANCHORSEARCHPIPELINE_H
