package org.opencv.android;

import android.graphics.Canvas;
import android.graphics.Point;

public interface CustomDrawer {
    void onDraw(Canvas canvas, Point orig_offset, Point orig_size);
}
